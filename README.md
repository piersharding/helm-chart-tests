helm-chart-tests
================

This is an example of Helm Chart inheritance.


Building Packages
-----------------

** NOT REQUIRED NOW **
In one window run the simple server for the Helm Chart repository:
```
$ make server
```

In another window, build the packages:
```
$ make package
```

Running Tests
-------------

Run the tests to show switching on and off sub-charts:
```
$ make tests
```

Using Tags with Conditions
--------------------------

Run the tests to show switching on and off sub-charts using tags and conditions:
```
$ make tags
```


Helm  Chart Structure
-----------------------
```
A-\
|  B-\
|-\   D*
|  C-\
|-\   D-\
  D*  |  E
      |-\
         F
```

SET ?=
PACKAGE ?= a
CHARTS_DIR ?= ./charts

.DEFAULT_GOAL := help

.PHONY: package server show help

clean: ## clean out references to chart tgz's
	rm -f $(CHARTS_DIR)/*/charts/*.tgz $(CHARTS_DIR)/*/Chart.lock ./repository/*

package: clean ## package all existing charts into a git based repo - run make server in another window
	mkdir -p repository
	@BASE=$$(pwd);\
	for i in f e d c b a; do \
	echo "Processing $(CHARTS_DIR)/$${i}"; \
	cd $${BASE}; \
	helm package $(CHARTS_DIR)/$${i} --dependency-update --repository-config=$${BASE}/repository/repositories.yaml --destination ./repository || true ; \
	cd $${BASE}/repository && helm repo index .; \
	done

# server: ## start python Server - run in another window before make package
# 	mkdir -p repository
# 	cd repository && python -m SimpleHTTPServer 8000

show: ## show the combined templates - pass eg: SET='--set="b.enabled=true"' to set values
	@printf '------------------------------------------------------------\n\n'
	helm template $(CHARTS_DIR)/$(PACKAGE) $(SET)
	@printf '------------------------------------------------------------\n\n'

tests: ## show different combinations of packages in the hierarchy
	make show PACKAGE=c
	make show SET='--set="d.enabled=false"' PACKAGE=c
	make show PACKAGE=b
	make show SET='--set="d.enabled=false"' PACKAGE=b
	make show PACKAGE=a
	make show SET='--set="c.d.enabled=false"' PACKAGE=a
	make show SET='--set="global.d.enabled=false"' PACKAGE=a

tags:
	make package CHARTS_DIR=./charts_with_tags
	@printf '\n\n\n Setting: nothing for c shows d as well !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags PACKAGE=c
	@printf '\n\n\n Setting: tags.d-tag=false for c suppresses d !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags SET='--set="tags.d-tag=false"' PACKAGE=c
	@printf '\n\n\n Setting: nothing for b shows d as well !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags PACKAGE=b
	@printf '\n\n\n Setting: tags.d-tag=false for b suppresses d !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags SET='--set="tags.d-tag=false"' PACKAGE=b
	@printf '\n\n\n Setting: nothing for a shows d at all levels !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags PACKAGE=a
	@printf '\n\n\n Setting: c.tags.d-tag=false shows nothing!!! \n\n'
	make show CHARTS_DIR=./charts_with_tags SET='--set="c.tags.d-tag=false"' PACKAGE=a
	@printf '\n\n\n Setting: tags.d-tag=false for a suppresses d at all levels !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags SET='--set="tags.d-tag=false"' PACKAGE=a
	@printf '\n\n\n Setting: tags.d-tag=false and d.enabled=true for a suppresses d except for a level !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags SET='--set="tags.d-tag=false,d.enabled=true"' PACKAGE=a
	@printf '\n\n\n Setting: tags.d-tag=false and c.d.enabled=true for a suppresses d except for c sub-chart d !!! \n\n'
	make show CHARTS_DIR=./charts_with_tags SET='--set="tags.d-tag=false,c.d.enabled=true"' PACKAGE=a

help:  ## show this help.
	@grep -hE '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
